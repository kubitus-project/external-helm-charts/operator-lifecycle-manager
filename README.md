# External Helm charts

## Operator Lifecycle Manager

Usage:

```shell
helm repo add operator-lifecycle-manager https://gitlab.com/api/v4/projects/29255838/packages/helm/stable

helm install operator-lifecycle-manager/olm
```

Available values are [listed here](https://github.com/operator-framework/operator-lifecycle-manager/blob/master/deploy/chart/values.yaml).
